<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
      <body style="font-family: system-ui; font-size: 14pt; background-color: FFEBFF">
        <div style="background-color: B275B9; color: 21211B;">
          <h2>DEVICES</h2>
        </div>
        <table border="3">
          <tr bgcolor="#B275B9">
            <th>Device ID</th>
            <th>Device group</th>
            <th>Title</th>
            <th>Price</th>
            <th>Country</th>
            <th>Ports</th>
            <th>Energy consumption</th>
            <th>Critical</th>
            <th>Peripheral</th>
            <th>Cooler</th>
          </tr>

          <xsl:for-each select="devices/device">
            <tr>
              <td><xsl:value-of select="count(preceding-sibling::*)+1" /></td>
              <td><xsl:value-of select="types/deviceGroup"/></td>
              <td><xsl:value-of select="name"/></td>
              <td>
                <xsl:value-of select="price"/>
                <xsl:text> UAH</xsl:text>
              </td>
              <td><xsl:value-of select="origin"/></td>
              <td>
                <xsl:for-each select="types/ports/port">
                  <xsl:value-of select="."/>
                  <xsl:text>, </xsl:text>
                </xsl:for-each>
                </td>
              <td>
                <xsl:value-of select="types/energyConsumption"/>
                <xsl:text>W</xsl:text>
              </td>
              <td>
                <xsl:choose>
                  <xsl:when test="critical='true'">
                  <xsl:text>YES</xsl:text>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:text>NO</xsl:text>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <td>
                <xsl:choose>
                  <xsl:when test="types/peripheral='true'">
                    <xsl:text>YES</xsl:text>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:text>NO</xsl:text>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <td>
                <xsl:choose>
                  <xsl:when test="types/cooler='true'">
                    <xsl:text>YES</xsl:text>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:text>NO</xsl:text>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
            </tr>
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>